#pragma once
#include <qserialport.h>
#include <qvector.h>


namespace Spring
{
   class SerialPortReader:QObject
   {
      Q_OBJECT

   public:
      SerialPortReader(QObject* parent, 
                       const QString& name, 
                       const QString& baudRate, 
                       const QString& dataBits,
                       const QString& parity, 
                       const QString& stopBits, 
                       const QString& flowControl);
      ~SerialPortReader();


      QVector<double> vecGetData();
      void start();
      void stop();
   public slots:
      void writeData();
      void readData();

   private:
      bool mReading=false;
      QSerialPort* mSerialPort;
      QVector<double> mValues;
      unsigned int mSampleRate;

      QByteArray mReadData;
      QString mBuffer="";

   };
}
