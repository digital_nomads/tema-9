set(INCROOT ${PROJECT_SOURCE_DIR}/include/Spring/Framework)
set(SRCROOT ${PROJECT_SOURCE_DIR}/src/Spring/Framework)

# all source files
file(GLOB INCS "${INCROOT}/*.h")
file(GLOB SRCS "${SRCROOT}/*.cpp")

#set(Qt5_DIR "C:/Qt/5.9.2/msvc2017_64/lib/cmake/Qt5")

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${Spring_SOURCE_DIR}/bin")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${Spring_SOURCE_DIR}/bin")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${Spring_SOURCE_DIR}/bin")

# Tell CMake to run moc when necessary:
set(CMAKE_AUTOMOC ON)

# Find the QtWidgets library
find_package(Qt5 COMPONENTS Core Widgets REQUIRED)

# As moc files are generated in the binary dir, tell CMake
# to always look for includes there:
include_directories("${Spring_SOURCE_DIR}/bin")

add_library(Framework SHARED ${SRCS} ${INCS})

target_link_libraries(Framework Qt5::Widgets Qt5::Core)