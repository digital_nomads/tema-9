#include "spring\Application\SerialPortReader.h"
#include "qdebug.h"
#include <qendian.h>
#include <QMetaEnum>
namespace Spring
{
   SerialPortReader::SerialPortReader(QObject* parent,
                                      const QString& name,
                                      const QString& baudRate, 
                                      const QString& dataBits, 
                                      const QString& parity, 
                                      const QString& stopBits, 
                                      const QString& flowControl)
   {

      mSerialPort = new QSerialPort(parent);

      //getting the enum type from qstring
      QMetaEnum metaEnum = QMetaEnum::fromType<QSerialPort::DataBits>();
      int dataBitsEnum = metaEnum.keyToValue(dataBits.toStdString().c_str());

      metaEnum=QMetaEnum::fromType<QSerialPort::BaudRate>();
      int baudRateEnum= metaEnum.keyToValue(baudRate.toStdString().c_str());

      metaEnum= QMetaEnum::fromType<QSerialPort::Parity>();
      int parityEnum=metaEnum.keyToValue(parity.toStdString().c_str());

      metaEnum = QMetaEnum::fromType<QSerialPort::StopBits>();
      int stopBitsEnum= metaEnum.keyToValue(stopBits.toStdString().c_str());

      metaEnum= QMetaEnum::fromType<QSerialPort::FlowControl>();
      int flowControlEnum= metaEnum.keyToValue(flowControl.toStdString().c_str());

      mSerialPort->setPortName("COM15");
      mSerialPort->open(QSerialPort::ReadOnly);
      mSerialPort->setBaudRate(baudRateEnum);
      mSerialPort->setDataBits((QSerialPort::DataBits)dataBitsEnum);
      mSerialPort->setParity((QSerialPort::Parity)parityEnum);
      mSerialPort->setStopBits((QSerialPort::StopBits)stopBitsEnum);
      mSerialPort->setFlowControl((QSerialPort::FlowControl)flowControlEnum);

      connect(mSerialPort, SIGNAL(readyRead()), this, SLOT(readData()));
      mValues.resize(500);
   }
   SerialPortReader::~SerialPortReader()
   {
      mSerialPort->close();
      delete mSerialPort;
   }

   void SerialPortReader::writeData()
   {}

   void SerialPortReader::readData()
   {
      if (mReading) {
         QStringList bufferList = mBuffer.split(",");

         if (bufferList.length() < 10) {
            mReadData = mSerialPort->readAll();
            mBuffer += QString::fromStdString(mReadData.toStdString());
         } else {
            while (mValues.size() > 500) {
               mValues.remove(0);
            }
            for (int i = 0; i < bufferList.length(); i++) {
               mValues.append(bufferList.at(i).toDouble());
            }

            // the last value is always 0 and not needed
            mValues.pop_back();
            mBuffer = "";
         }
      }
   }

   QVector<double> SerialPortReader::vecGetData()
   {
      return mValues;
   }

   void SerialPortReader::start()
   {
      mReading = true;
   }

   void SerialPortReader::stop()
   {
      mReading = false;
   }


}

