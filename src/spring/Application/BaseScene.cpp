#include <spring\Application\BaseScene.h>

#include "ui_base_scene.h"

namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}
	void BaseScene::createScene()
	{
		//create the UI
		const auto ui = std::make_shared<Ui_baseScene>();
		ui->setupUi(m_uMainWindow.get());

    //connect btn's release signal to defined slot
    QObject::connect(ui->backButton, SIGNAL(released()), this, SLOT(mp_BackButton()));
    QObject::connect(ui->startButton, SIGNAL(released()), this, SLOT(mf_StartTimer()));
    QObject::connect(ui->stopButton, SIGNAL(released()), this, SLOT(mf_StopTimer()));
    QObject::connect(ui->backButton, SIGNAL(released()), this, SLOT(mf_StopTimer()));

		//setting a temporary new title
		m_uMainWindow->setWindowTitle(QString("new title goes here"));

		//setting centralWidget
		centralWidget = ui->centralwidget;

		//Get the title from transient data
		std::string appName = boost::any_cast<std::string>(m_TransientDataCollection["ApplicationName"]);

		m_uMainWindow->setWindowTitle(QString(appName.c_str()));

      mv_customPlot = ui->widget;//todo

      mp_InitPlotters();

      mv_dRefreshRate = boost::any_cast<double>(m_TransientDataCollection["RefreshRate"]);

      mv_Timer.setInterval(1000/mv_dRefreshRate);

      QObject::connect(&mv_Timer, SIGNAL(timeout()), this, SLOT(mp_PlotRandom()));

      unsigned int lv_nSampleRate  = boost::any_cast<unsigned int>(m_TransientDataCollection["SampleRate"]);

      mMonoInput = new MonoInput(lv_nSampleRate);

      QString portName = boost::any_cast<QString>(m_TransientDataCollection.find("PortName")->second);
      QString baudRate = boost::any_cast<QString>(m_TransientDataCollection.find("BaudRate")->second);
      QString dataBits = boost::any_cast<QString>(m_TransientDataCollection.find("DataBits")->second);
      QString parity = boost::any_cast<QString>(m_TransientDataCollection.find("Parity")->second);
      QString stopBits = boost::any_cast<QString>(m_TransientDataCollection.find("StopBits")->second);
      QString flowControl = boost::any_cast<QString>(m_TransientDataCollection.find("FlowControl")->second);

      mSerialPortReader = new SerialPortReader(this,portName,baudRate,dataBits,parity,stopBits,flowControl);     
	}

   void BaseScene::mp_InitPlotters()
   {
      //time plot
      mv_customPlot->setInteraction(QCP::iRangeZoom, true);
      mv_customPlot->setInteraction(QCP::iRangeDrag, true);

      mv_customPlot->addGraph();
      mv_customPlot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);

      mv_customPlot->xAxis->setLabel("sec");
      mv_customPlot->yAxis->setLabel("light");
   }

   void BaseScene::mp_PlotRandom()
   {
      //QVector<double> values = mMonoInput->vecGetData();
      QVector<double> values = mSerialPortReader->vecGetData();

      const int lc_nNoOfSamples = values.length();
      QVector<double> y(lc_nNoOfSamples);
      QVector<double> x(lc_nNoOfSamples);

      for (int i = 0; i < lc_nNoOfSamples; i++) {
         x[i] = i;
         //y[i] = rand() % 20;

      }

      mv_customPlot->graph(0)->setData(x, values);
      mv_customPlot->rescaleAxes();
      mv_customPlot->replot();
   }

   void BaseScene::mp_PlotClean()
   {
      mv_customPlot->graph(0)->data()->clear();
   }

   void BaseScene::mf_StartTimer()
   {
      mAudioInput = new QAudioInput(mMonoInput->getAudioFormat());
      mMonoInput->open(QIODevice::WriteOnly);
      mAudioInput->start(mMonoInput);

      mSerialPortReader->start();

      mv_Timer.start();
   }

   void BaseScene::mf_StopTimer()
   {
      mAudioInput->stop();
      mMonoInput->close();

      mSerialPortReader->stop();

      mv_Timer.stop();
      mp_PlotClean();
   }
   

	void BaseScene::release()
	{
      mf_StopTimer();
      delete mMonoInput;
		delete centralWidget;
      delete mSerialPortReader;
	}

	BaseScene::~BaseScene()
	{

	}

  void BaseScene::mp_BackButton()
  {
    const std::string c_szNextSceneName = "Initial scene";
    emit SceneChange(c_szNextSceneName);
  }

}
