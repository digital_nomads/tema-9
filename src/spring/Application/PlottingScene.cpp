#include <spring\Application\PlottingScene.h>

#include "ui_plotting_scene.h"
#include "aquila/aquila.h"
#include "Iir.h"

namespace Spring
{
	PlottingScene::PlottingScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void PlottingScene::createScene()
	{
		// Create the UI
		const auto ui = std::make_shared<Ui_plottingScene>();
		ui->setupUi(m_uMainWindow.get());

    // Connect btn's release signal to defined slot
    QObject::connect(ui->backButton, SIGNAL(released()), this, SLOT(mp_BackButton()));

		// Setting a temporary new title
		m_uMainWindow->setWindowTitle(QString("Plotting Example"));

    mv_customPlotTime = ui->customPlotTime;
    mv_customPlotFreq = ui->customPlotFreq;

    // Connect btn's release signal to defined slot
    QObject::connect(ui->plotButton, SIGNAL(released()), this, SLOT(mp_OnPlotButton()));

		// Setting centralWidget
		centralWidget = ui->centralwidget;

    // Populate combobox
    mv_signalComboBox = ui->SignalComboBox;
    mv_signalComboBox->addItem("Sine");
    mv_signalComboBox->addItem("SineSum");
    mv_signalComboBox->addItem("Square");
    mv_signalComboBox->addItem("Sweep");
    mv_signalComboBox->addItem("Dirac");
	mv_signalComboBox->addItem("SineSumFiltered");

  
    // Initialize plotters
    mp_InitPlotters();
	}

  void PlottingScene::mp_InitPlotters()
  {
    // Time plots
    mv_customPlotTime->setInteraction(QCP::iRangeZoom, true);
    mv_customPlotTime->setInteraction(QCP::iRangeDrag, true);

    mv_customPlotTime->addGraph();
    mv_customPlotTime->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
    
    mv_customPlotTime->xAxis->setLabel("SAMPLES");
    mv_customPlotTime->yAxis->setLabel("AMPLITUDE");

    // Freq plot
    mv_customPlotFreq->setInteraction(QCP::iRangeZoom, true);
    mv_customPlotFreq->setInteraction(QCP::iRangeDrag, true);

    mv_customPlotFreq->addGraph();
    mv_customPlotFreq->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);

    mv_customPlotFreq->xAxis->setLabel("FREQUENCY (Hz)");
    mv_customPlotFreq->yAxis->setLabel("AMPLITUDE");
    mv_customPlotFreq->xAxis->setRange(0, 1000);
    mv_customPlotFreq->yAxis->setRange(-0.2, 2);
  }


  void PlottingScene::release()
	{
		delete centralWidget;
	}

	PlottingScene::~PlottingScene()
	{
	}

  double SineWave(double t, double amplitude, double frequency)
  {
    return amplitude * sin(2 * M_PI * frequency * t);
  }

  void PlottingScene::mp_BackButton()
  {
    const std::string c_szNextSceneName = "Initial scene";
    emit SceneChange(c_szNextSceneName);
  }

  int sgn(double x)
  {
	  return (0 < x) - (x < 0);
  }

  double Sweep(double f_start, double f_end, double interval, int steps, double x)
  {
	  double delta = x / (float)steps;
	  double t = interval * delta;
	  double phase = 2 * M_PI * t * (f_start + (f_end - f_start) * delta / 2);
	  return (3 * sin(phase));
  }


  static const unsigned int SAMPLE_RATE = 81920; // Hz
  static const       double PLOT_TIME = 0.1;     // seconds
  static const       double AMPLITUDE = 1;

  static const unsigned int NO_OF_SAMPLES = PLOT_TIME * SAMPLE_RATE;

  void PlottingScene::mp_OnPlotButton()
  {
    QString comboSelection = mv_signalComboBox->currentText();
    
	// Set xAxis
	QVector<double> y(NO_OF_SAMPLES);
	QVector<double> x(NO_OF_SAMPLES);
	x[0] = 0;
	for (int i = 1; i < NO_OF_SAMPLES; i++)
	{
		x[i] = i;
	}

    if (comboSelection == "Sine")
    {
      // Set yAxis
      for (int i = 0; i < NO_OF_SAMPLES; i++)
      {
        double xval = i / (double)NO_OF_SAMPLES;
		double val = SineWave(xval, AMPLITUDE, 20);
		y[i] = val;
      }
    }
    else if (comboSelection == "SineSum")
    {
		for (int i = 0; i < NO_OF_SAMPLES; i++)
		{
			double xval = i / (double)NO_OF_SAMPLES;
			double val = SineWave(xval, AMPLITUDE, 10) + SineWave(xval, AMPLITUDE / 10, 1000);
			y[i] = val;
		}
    }
    else if (comboSelection == "Square")
    {
		// Set yAxis
		for (int i = 0; i < NO_OF_SAMPLES; i++)
		{
			double xval = i / (double)NO_OF_SAMPLES;
			double val = sgn(SineWave(xval, AMPLITUDE, 20));
			y[i] = val;
		}
    }
    else if (comboSelection == "Sweep")
    {
		Iir::Butterworth::LowPass<3> f;
		const float samplingrate = SAMPLE_RATE; // Hz
		const float cutoff_frequency = 1000; // Hz
		f.setup(3, samplingrate, cutoff_frequency);
		// Set yAxis
		for (int i = 0; i < NO_OF_SAMPLES; i++)
		{
			y[i] = f.filter(Sweep(100,2000,0.1, NO_OF_SAMPLES, i));
		}
    }
    else if (comboSelection == "Dirac")
    {
		for (int i = 0; i < NO_OF_SAMPLES; i++)
		{
			if (i == 100 || i == 800)
				y[i] = 1000;
			else
				y[i] = 0;

		}
    }

	else if (comboSelection == "SineSumFiltered")
	{
		Iir::Butterworth::LowPass<5> f;
		const float samplingrate = SAMPLE_RATE; // Hz
		const float cutoff_frequency = 900; // Hz
		f.setup(5, samplingrate, cutoff_frequency);

		for (int i = 0; i < NO_OF_SAMPLES; i++)
		{
			double xval = i / (double)NO_OF_SAMPLES;
			double val = SineWave(xval, AMPLITUDE, 10) + SineWave(xval, AMPLITUDE / 10, 1000);
			y[i] = f.filter(val);
		}
	}

	// FFT
	QVector<double> yFFT(NO_OF_SAMPLES);

	auto fft = Aquila::FftFactory::getFft(NO_OF_SAMPLES);
	Aquila::SpectrumType spectrum = fft->fft(y.data());

	for (int i = 0; i < NO_OF_SAMPLES; i++)
	{
		yFFT[i] = std::abs(spectrum[i]) / (NO_OF_SAMPLES / 2);
	}

	mv_customPlotTime->graph(0)->setData(x, y);
	mv_customPlotTime->rescaleAxes();
	mv_customPlotTime->replot();

	mv_customPlotFreq->graph(0)->setData(x, yFFT);
	mv_customPlotFreq->replot();
  }
}
